package glog

import "github.com/fatih/color"

var (
	color_get     = "[GET]    "
	color_post    = "[POST]   "
	color_put     = "[PUT]    "
	color_patch   = "[PATCH]  "
	color_delete  = "[DELETE] "
	color_options = "[OPTIONS]"
	color_group   = ""
	color_api     = "[API]"
)

func getColor(method string) (string, color.Attribute) {
	method_data := ""
	color_data := color.FgWhite
	if method == "get" {
		method_data = color_get
		color_data = color.FgGreen
	} else if method == "post" {
		method_data = color_post
		color_data = color.FgYellow
	} else if method == "put" {
		method_data = color_put
		color_data = color.FgBlue
	} else if method == "patch" {
		method_data = color_patch
		color_data = color.FgMagenta
	} else if method == "delete" {
		method_data = color_delete
		color_data = color.FgRed
	} else if method == "default" {
		method_data = color_group
		color_data = color.FgCyan
	} else if method == "api" {
		method_data = color_api
		color_data = color.FgHiMagenta
	} else {
		method_data = color_options
		color_data = color.FgBlack
	}
	return method_data, color_data
}
