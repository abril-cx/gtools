package glog

import (
	"errors"
	"fmt"
	"os"
	"runtime"
	"strings"

	utils "gitee.com/abril-cx/gtools/gutils"

	"github.com/fatih/color"
)

var IS_DEBUG = false

func prefix() string {
	date_time := utils.GetDateTime(0)
	path, _ := os.Getwd()
	dir_list := strings.Split(path, "/")
	dir_name := dir_list[len(dir_list)-1]
	_, file_path, line, _ := runtime.Caller(2)
	file_path = strings.ReplaceAll(file_path, path, dir_name)
	return date_time + " " + file_path + ":" + fmt.Sprint(line)
}

func Primary(data ...interface{}) {
	str := "[PRIMARY]"
	detail := prefix()
	data_str := ""
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	info_type := color.New(color.Bold, color.FgBlue).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgBlue).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgBlue).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
}

func Cyan(data ...interface{}) {
	str := "[CYAN]"
	detail := prefix()
	data_str := ""
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	info_type := color.New(color.Bold, color.FgCyan).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgCyan).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgCyan).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
}

func Success(data ...interface{}) {
	str := "[SUCCESS]"
	detail := prefix()
	data_str := ""
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	info_type := color.New(color.Bold, color.FgGreen).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgGreen).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgGreen).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
}

func Default(data ...interface{}) {
	str := "[DEFAULT]"
	detail := prefix()
	data_str := ""
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	info_type := color.New(color.Bold).PrintfFunc()
	info_type(str)
	detail_style := color.New().PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New().PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
}

func Info(data ...interface{}) {
	str := "[INFO]"
	detail := prefix()
	data_str := ""
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	writeFile("info", str+detail+" > "+data_str[:len(data_str)-4])
	info_type := color.New(color.Bold, color.FgHiBlack).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgHiBlack).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgHiBlack).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
}

func Warning(data ...interface{}) {
	str := "[WARNING]"
	detail := prefix()
	data_str := ""
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	writeFile("warning", str+detail+" > "+data_str[:len(data_str)-4])
	info_type := color.New(color.Bold, color.FgYellow).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgYellow).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgYellow).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
}

func Error(data ...interface{}) {
	str := "[ERROR]"
	detail := prefix()
	data_str := ""
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	writeFile("error", str+detail+" > "+data_str[:len(data_str)-4])
	info_type := color.New(color.Bold, color.FgRed).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgRed).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgRed).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
}

func InfoReturn(errStr string, data ...interface{}) error {
	str := "[INFO]"
	detail := prefix()
	data_str := ""
	data_str += errStr + " :: "
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	writeFile("info", str+detail+" > "+data_str[:len(data_str)-4])
	info_type := color.New(color.Bold, color.FgHiBlack).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgHiBlack).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgHiBlack).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
	return errors.New(errStr)
}

func WarningReturn(errStr string, data ...interface{}) error {
	str := "[WARNING]"
	detail := prefix()
	data_str := ""
	data_str += errStr + " :: "
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	writeFile("warning", str+detail+" > "+data_str[:len(data_str)-4])
	info_type := color.New(color.Bold, color.FgYellow).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgYellow).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgYellow).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
	return errors.New(errStr)
}

func ErrorReturn(errStr string, data ...interface{}) error {
	str := "[ERROR]"
	detail := prefix()
	data_str := ""
	data_str += errStr + " :: "
	for _, v := range data {
		data_str += fmt.Sprint(v) + " :: "
	}
	writeFile("error", str+detail+" > "+data_str[:len(data_str)-4])
	info_type := color.New(color.Bold, color.FgRed).PrintfFunc()
	info_type(str)
	detail_style := color.New(color.FgRed).PrintfFunc()
	detail_style(detail + " > ")
	data_style := color.New(color.FgRed).PrintlnFunc()
	data_style(data_str[:len(data_str)-4])
	return errors.New(errStr)
}

func Debug(data ...interface{}) {
	if IS_DEBUG {
		str := "[DEBUG]"
		detail := prefix()
		data_str := ""
		for _, v := range data {
			data_str += fmt.Sprint(v) + " :: "
		}
		info_type := color.New(color.Bold, color.FgMagenta).PrintfFunc()
		info_type(str)
		detail_style := color.New(color.FgMagenta).PrintfFunc()
		detail_style(detail + " > ")
		data_style := color.New(color.FgMagenta).PrintlnFunc()
		data_style(data_str[:len(data_str)-4])
	}
}

func Api(method string, url string, ip string, logApiPrint bool, logApiWrite bool) {
	date_time := utils.GetDateTime(0)
	str, col := getColor("api")
	if logApiWrite {
		writeFile("api", str+date_time+" :: ["+method+"] :: "+ip+" :: "+url)
	}
	if logApiPrint {
		_, col1 := getColor(strings.ToLower(method))
		type_style := color.New(color.Bold, col).PrintfFunc()
		type_style(str)
		date_time_style := color.New().PrintfFunc()
		date_time_style(date_time + " :: ")
		data_method_style := color.New(color.Bold, col1).PrintfFunc()
		data_method_style("[" + method + "]")
		data_url_style := color.New().PrintlnFunc()
		data_url_style(" :: " + ip + " :: " + url)
	}
}
