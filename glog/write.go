package glog

import (
	"bufio"
	"fmt"
	"os"

	utils "gitee.com/abril-cx/gtools/gutils"
)

func writeFile(file_name string, content string) {
	dates := utils.GetDateTimeFormat(0, "ym", "")
	dir_name := "./log/" + dates
	utils.CreateDir(dir_name)
	day := utils.GetDateTimeFormat(0, "d", "")
	filePath := dir_name + "/" + day + "-" + file_name + ".log"
	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("文件打开失败", err)
	}
	//及时关闭file句柄
	defer file.Close()
	//写入文件时，使用带缓存的 *Writer
	write := bufio.NewWriter(file)
	write.WriteString(content + "\n")
	//Flush将缓存的文件真正写入到文件中
	write.Flush()
}
