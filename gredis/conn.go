package gredis

import (
	// "strconv"
	"errors"
	"strings"
	"time"

	log "gitee.com/abril-cx/gtools/glog"
	"github.com/gomodule/redigo/redis"
)

var (
	Prefix = "gciobe"
	Host   = "127.0.0.1"
	Port   = "6379"
)

// 全局变量, 外部使用utils.RedisConn来访问
var Conn redisUtil

type redisUtil struct {
	client *redis.Pool
}

// 初始化redis
func Init() {
	Conn.client = &redis.Pool{
		MaxIdle:     100,
		MaxActive:   12000,
		IdleTimeout: time.Duration(180),
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial(
				"tcp",
				Host+":"+Port,
				// redis.DialPassword("xxxxxx"),
				redis.DialReadTimeout(time.Second),
				redis.DialWriteTimeout(time.Second),
			)
			if err != nil {
				log.Error("redisClient dial host: %s, auth: %s err: %s", "127.0.0.1:6379", "xxxxxx", err.Error())
				return nil, err
			}
			return c, nil
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			if err != nil {
				log.Error("redisClient ping err: %s", err.Error())
			}
			return err
		},
	}
}

// 设置数据到redis中（string）
func (rs *redisUtil) SetStr(key string, value string) error {
	_key := Prefix + "_" + key
	key_list := strings.Split(key, "_")
	if strings.Contains(key_list[0], "be") {
		_key = key
	}
	conn, err := rs.client.Dial()
	conn.Do("Set", _key, value)
	return err
}

// //设置数据到redis中（string）
// func (rs *redisUtil) SetStrTime(key string, value string, expireSecond int) bool {
// 	val, err := rs.client.Do("SET", key, value, "EX", expireSecond, "NX")
// 	if err != nil || val == nil {
// 		return false
// 	}
// 	return true
// }

// 设置数据到redis中（string）
func (rs *redisUtil) SetStrTime(key string, value string, expireSecond int) error {
	_key := Prefix + "_" + key
	key_list := strings.Split(key, "_")
	if strings.Contains(key_list[0], "be") {
		_key = key
	}
	conn, err := rs.client.Dial()
	conn.Do("Set", _key, value, "ex", expireSecond)
	return err
}

// 获取redis中数据（string）
func (rs *redisUtil) GetStr(key string) (string, error) {
	_key := Prefix + "_" + key
	key_list := strings.Split(key, "_")
	if strings.Contains(key_list[0], "be") {
		_key = key
	}
	conn, err := rs.client.Dial()
	if err != nil {
		return "", err
	}
	val, err := conn.Do("Get", _key)
	if err != nil {
		return "", err
	}
	if val == nil {
		return "", errors.New("redis key not exist")
	}
	result := string(val.([]byte))
	if result == "" {
		return "", errors.New("redis key not exist")
	}
	return result, nil
}

// //设置数据到redis中（hash）
// func (rs *redisUtil) HSet(key string, field string, value string) error {
// 	_, err := rs.client.Do("HSet", key, field, value)
// 	return err
// }

// //设置数据到redis中（hash）
// func (rs *redisUtil) HGet(key string, field string) (string, error) {
// 	val, err := rs.client.Do("HGet", key, field)
// 	if err != nil {
// 		return "", err
// 	}
// 	return string(val.([]byte)), nil
// }

// //设置数据到redis中（hash）
// func (rs *redisUtil) SetList(key string, listData []string, expireSecond int) []string {
// 	resData := []string{}
// 	val, err := rs.client.Do("LPush", key, listData)
// 	if err != nil {
// 		return resData
// 	}
// 	fmt.Println(val)
// 	// return string(val.([]byte)), nil
// 	return resData
// }

// //设置数据到redis中（hash）
// func (rs *redisUtil) GetList(key string) []string {
// 	resData := []string{}
// 	val, err := rs.client.Do("LRange", key, 0, -1)
// 	if err != nil {
// 		return resData
// 	}
// 	fmt.Println(val)
// 	// return string(val.([]byte)), nil
// 	return resData
// }

// 删除
func (rs *redisUtil) DelByKey(key string) error {
	_key := Prefix + "_" + key
	key_list := strings.Split(key, "_")
	if strings.Contains(key_list[0], "be") {
		_key = key
	}
	conn, err := rs.client.Dial()
	conn.Do("DEL", _key)
	return err
}

// //设置key过期时间
// func (rs *redisUtil) SetExpire(key string, expireSecond int) error {
// 	_, err := rs.client.Do("EXPIRE", key, expireSecond)
// 	return err
// }
