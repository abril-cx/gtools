package gResResponse

import "github.com/gin-gonic/gin"

func Success(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"code":    1000,
		"data":    nil,
		"message": nil,
	})
}

func SuccessData(ctx *gin.Context, data interface{}) {
	ctx.JSON(200, gin.H{
		"code":    1000,
		"data":    data,
		"message": nil,
	})
}

func SuccessText(ctx *gin.Context, data interface{}) {
	ctx.String(200, data.(string))
}

func SuccessFile(ctx *gin.Context, filePath string) {
	ctx.File(filePath)
}

// func SuccessApiPage(req *ghttp.Request, data interface{}, pagination interface{}) {
// 	req.Response.WriteJson(map[string]interface{}{
// 		"code": 1000,
// 		"data": map[string]interface{}{
// 			"list":       data,
// 			"pagination": pagination,
// 		},
// 		"message": nil,
// 	})
// }

// func SuccessApiList(req *ghttp.Request, data interface{}) {
// 	req.Response.WriteJson(map[string]interface{}{
// 		"code":    1000,
// 		"data":    data,
// 		"message": nil,
// 	})
// }

func Info(ctx *gin.Context, message interface{}) {
	ctx.JSON(200, gin.H{
		"code":    501,
		"data":    nil,
		"message": message,
	})
}

func Error(ctx *gin.Context, message interface{}) {
	ctx.JSON(200, gin.H{
		"code":    502,
		"data":    nil,
		"message": message,
	})
}

// 参数缺失
func ErrorDataMiss(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"code":    502,
		"data":    nil,
		"message": "参数缺失",
	})
}

// 参数异常
func ErrorDataNil(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"code":    502,
		"data":    nil,
		"message": "参数异常",
	})
}

// 接口未启用
func ErrorNotEnable(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"code":    501,
		"data":    nil,
		"message": "当前接口未启用",
	})
}

// 登陆已失效，请重新登陆！
func Logout(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"code":    401,
		"data":    nil,
		"message": "登陆已失效，请重新登陆！",
	})
}

// func File(req *ghttp.Request, file_path string) {
// 	req.Response.ServeFile(file_path)
// }

// func FileDown(req *ghttp.Request, file_path string, down_file_name string) {
// 	req.Response.ServeFileDownload(file_path, down_file_name)
// }

// func Logout(req *ghttp.Request) {
// 	req.Response.WriteJson(map[string]interface{}{
// 		"code":    401,
// 		"data":    nil,
// 		"message": "登陆已失效，请重新登陆！",
// 	})
// }
