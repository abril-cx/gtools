package gcapt

import (
	"strings"

	"gitee.com/abril-cx/gtools/gcapt/svg"
	utils "gitee.com/abril-cx/gtools/gutils"
)

func GetCaptSvg() (string, string) {
	code := utils.GetRandNum(4)
	code_list := strings.Split(code, "")
	svg_list := []string{}
	for i, v := range code_list {
		svg_list = append(svg_list, svg.SVG_MAP[v][i])
	}
	line1 := `<path d="M` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + ` C` + utils.GetRandNum(2) + ` 29,` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + `,158 ` + utils.GetRandNum(2) + `" stroke="#2c3142" fill="none"></path>`
	line2 := `<path d="M` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + ` C` + utils.GetRandNum(2) + ` 29,` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + `,158 ` + utils.GetRandNum(2) + `" stroke="#2c3142" fill="none"></path>`
	line3 := `<path d="M` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + ` C` + utils.GetRandNum(2) + ` 29,` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + `,158 ` + utils.GetRandNum(2) + `" stroke="#2c3142" fill="none"></path>`
	line4 := `<path d="M` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + ` C` + utils.GetRandNum(2) + ` 29,` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + `,158 ` + utils.GetRandNum(2) + `" stroke="#2c3142" fill="none"></path>`
	end_line := `<path d="M` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + ` C` + utils.GetRandNum(2) + ` 29,` + utils.GetRandNum(2) + ` ` + utils.GetRandNum(2) + `,158 ` + utils.GetRandNum(2) + `" stroke="#2c3142" fill="none"></path></svg>`
	res_svg_data := svg.SVG_DATA + strings.Join(svg_list, "") + line1 + line2 + line3 + line4 + end_line
	return code, strings.ReplaceAll(res_svg_data, "\"", "'")
}
