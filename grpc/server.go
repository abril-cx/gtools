package grpc

import (
	"net"
	"net/http"
	"net/rpc"
)

func ListenAndServer(be any, port string) error {
	host_port := "0.0.0.0:4242"
	if port != "" {
		host_port = "0.0.0.0:" + port
	}
	//将类实例化为对象
	// pd := new(test)
	//服务端注册一个对象，该对象就作为一个服务被暴露出去
	err := rpc.Register(be)
	// err := rpc.Register
	if err != nil {
		return err
	}
	//连接到网络
	rpc.HandleHTTP()

	//监听端口
	ln, err := net.Listen("tcp", host_port)
	if err != nil {
		return err
	}
	http.Serve(ln, nil)
	return nil
}

// 使用例子

// type RpcServer struct{}

// func (s *RpcServer) GetUserInfo(params map[string]interface{}, result *map[string]interface{}) error {
// 	*result = map[string]interface{}{
// 		"result": "result",
// 		"zxc":    "123",
// 	}
// 	return nil
// }

// func InitRpcServer() {
// 	grpc.ListenAndServer(&RpcServer{}, "4242")
// }
