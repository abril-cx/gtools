package grpc

import (
	"net/rpc"
)

func Client(serviceMethod string, params map[string]interface{}, host_port string) (map[string]interface{}, error) {
	if host_port == "" {
		host_port = "127.0.0.1:4242"
	}
	var res_data map[string]interface{}
	//建立网络连接
	cli, err := rpc.DialHTTP("tcp", host_port)
	if err != nil {
		return res_data, err
	}
	//客户端调用服务端GetInfo方法，并传递参数
	err = cli.Call(serviceMethod, params, &res_data)
	if err != nil {
		return res_data, err
	}
	err = cli.Close()
	if err != nil {
		return res_data, err
	}
	return res_data, nil
}

// 使用例子

// var service = "userbe"

// type RpcUserbe struct {
// 	Params map[string]interface{}
// 	Result map[string]interface{}
// 	Error  error
// }

// func (r *RpcUserbe) GetUserInfo() {
// 	r.Result, r.Error = grpc.Client("RpcServer.GetUserInfo", r.Params, "127.0.0.1:4242")
// }
