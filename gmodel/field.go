package gmodel

import "go.mongodb.org/mongo-driver/bson/primitive"

type Field struct {
	Id primitive.ObjectID `json:"_id" bson:"_id"`

	Source      string `json:"source" bson:"source"`
	Model       string `json:"model" bson:"model"`
	Field       string `json:"field" bson:"field"`
	Name        string `json:"name" bson:"name"`
	Types       string `json:"types" bson:"types"`
	Default     string `json:"default" bson:"default"`
	Required    bool   `json:"required" bson:"required"`
	Transmit    bool   `json:"transmit" bson:"transmit"`
	IsBind      bool   `json:"is_bind" bson:"is_bind"`
	Bind        string `json:"bind" bson:"bind"`
	Crypto      bool   `json:"crypto" bson:"crypto"`
	Module      string `json:"module" bson:"module"`
	ModuleShow  string `json:"module_show" bson:"module_show"`
	Multiple    bool   `json:"multiple" bson:"multiple"`
	Related     bool   `json:"related" bson:"related"` // 关联显示字段(不关联返回全部)
	Select      bool   `json:"select" bson:"select"`
	Hidden      bool   `json:"hidden" bson:"hidden"` // 列表不展示此列(可以勾选显示)
	HiddenDepth bool   `json:"hidden_depth" bson:"hidden_depth"`
	Sort        string `json:"sort" bson:"sort"`
	Span        int64  `json:"span" bson:"span"` // 表单宽度[ (1) ]
	Data        string `json:"data" bson:"data"`
	Tree        string `json:"tree" bson:"tree"` // 列表展示[ (text), img ]

	Remark  string `json:"remark" bson:"remark"`
	Created string `json:"created" bson:"created"`
	Updated string `json:"updated" bson:"updated"`
	Deleted string `json:"deleted" bson:"deleted"`
}

type FieldData struct {
	Id primitive.ObjectID `json:"_id" bson:"_id"`

	Model string `json:"model" bson:"model"`
	Field string `json:"field" bson:"field"`
	Value string `json:"value" bson:"value"`

	Remark  string `json:"remark" bson:"remark"`
	Created string `json:"created" bson:"created"`
	Updated string `json:"updated" bson:"updated"`
	Deleted string `json:"deleted" bson:"deleted"`
}
