package grouters

import (
	ResResponse "gitee.com/abril-cx/gtools/gResResponse"
	mongo "gitee.com/abril-cx/gtools/gmongo"
	views "gitee.com/abril-cx/gtools/gviews"
	"github.com/gin-gonic/gin"
)

type DefaultRouterInterface interface {
	setModelView(model string, view views.DefaultViewInterface)
	InfoFunc(ctx *gin.Context)
	AddFunc(ctx *gin.Context)
	UpdateFunc(ctx *gin.Context)
	PageFunc(ctx *gin.Context)
	ListFunc(ctx *gin.Context)
	SelectFunc(ctx *gin.Context)
	FieldFunc(ctx *gin.Context)
	UpperFunc(ctx *gin.Context)
	MoveFunc(ctx *gin.Context)
	ArchivalFunc(ctx *gin.Context)
	DeleteFunc(ctx *gin.Context)
}

type DefaultRouter struct {
	Model        string
	View         views.DefaultViewInterface
	HandleRouter HandleRouter
}

func (r *DefaultRouter) setModelView(model string, view views.DefaultViewInterface) {
	r.Model = model
	r.View = view
}

func (r *DefaultRouter) InfoFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data := r.HandleRouter.GetQueryData(ctx, r.Model)
	result, err := r.View.InfoFunc(req_data)
	if err != nil {
		ResResponse.Error(ctx, err.Error())
		return
	}
	ResResponse.SuccessData(ctx, result)
}

func (r *DefaultRouter) AddFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data, err := r.HandleRouter.GetBodyData(ctx, r.Model)
	if err != nil {
		ResResponse.ErrorDataNil(ctx)
		return
	}
	fields, err := mongo.GetFields(r.Model)
	if err != nil {
		// 获取字段失败
		ResResponse.Error(ctx, "获取字段失败")
		return
	}
	err = CheckRequired(fields, req_data)
	if err != nil {
		ResResponse.Error(ctx, "缺少必填字段")
		return
	}
	result, err := r.View.AddFunc(req_data)
	if err != nil {
		ResResponse.Error(ctx, "新建失败")
		return
	}
	ResResponse.SuccessData(ctx, result)
}

func (r *DefaultRouter) UpdateFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data, err := r.HandleRouter.GetBodyData(ctx, r.Model)
	if err != nil {
		ResResponse.ErrorDataNil(ctx)
		return
	}
	err = r.HandleRouter.CheckId(req_data)
	if err != nil {
		ResResponse.ErrorDataMiss(ctx)
		return
	}
	err = r.View.UpdateFunc(req_data)
	if err != nil {
		ResResponse.Error(ctx, err.Error())
		return
	}
	ResResponse.Success(ctx)
}

func (r *DefaultRouter) PageFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data_query := r.HandleRouter.GetQueryData(ctx, r.Model)
	// req_data, _ := r.HandleRouter.GetBodyData(ctx, r.Model)
	// req_data_query.Datas = req_data.Datas
	result, err := r.View.PageFunc(req_data_query)
	if err != nil {
		ResResponse.Error(ctx, err.Error())
		return
	}
	ResResponse.SuccessData(ctx, result)
}

func (r *DefaultRouter) ListFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data, _ := r.HandleRouter.GetBodyData(ctx, r.Model)
	result, err := r.View.ListFunc(req_data)
	if err != nil {
		ResResponse.Error(ctx, err.Error())
		return
	}
	ResResponse.SuccessData(ctx, result)
}

func (r *DefaultRouter) SelectFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data, _ := r.HandleRouter.GetBodyData(ctx, r.Model)
	result, err := r.View.SelectFunc(req_data)
	if err != nil {
		ResResponse.Error(ctx, err.Error())
		return
	}
	ResResponse.SuccessData(ctx, result)
}

func (r *DefaultRouter) FieldFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data, _ := r.HandleRouter.GetBodyData(ctx, r.Model)
	result, err := r.View.FieldFunc(req_data)
	if err != nil {
		ResResponse.Error(ctx, err.Error())
		return
	}
	ResResponse.SuccessData(ctx, result)
}

func (r *DefaultRouter) MoveFunc(ctx *gin.Context) {
	ResResponse.Success(ctx)
}

func (r *DefaultRouter) UpperFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data, _ := r.HandleRouter.GetBodyData(ctx, r.Model)
	result, err := r.View.UpperFunc(req_data)
	if err != nil {
		ResResponse.Error(ctx, err.Error())
		return
	}
	ResResponse.SuccessData(ctx, result)
}

func (r *DefaultRouter) ArchivalFunc(ctx *gin.Context) {
	ResResponse.Success(ctx)
}

func (r *DefaultRouter) DeleteFunc(ctx *gin.Context) {
	if r.Model == "" {
		ResResponse.ErrorNotEnable(ctx)
		return
	}
	req_data, err := r.HandleRouter.GetBodyData(ctx, r.Model)
	if err != nil {
		ResResponse.ErrorDataNil(ctx)
		return
	}
	err = r.HandleRouter.CheckId(req_data)
	if err != nil {
		ResResponse.ErrorDataMiss(ctx)
		return
	}
	err = r.View.DeleteFunc(req_data)
	if err != nil {
		ResResponse.Error(ctx, err.Error())
		return
	}
	ResResponse.Success(ctx)
}
