package grouters

import (
	"io/fs"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Embed 嵌入静态文件 path ( /futurebe/web/ ) 是嵌入的路径，fsys 是嵌入的文件系统
func Embed(server *gin.Engine, path string, fsys fs.FS) {
	server.GET("/futurebe/web/*filepath", func(c *gin.Context) {
		staticServer := http.FileServer(http.FS(fsys))
		staticServer.ServeHTTP(c.Writer, c.Request)
	})
}
