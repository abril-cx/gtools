package grouters

import (
	views "gitee.com/abril-cx/gtools/gviews"
	"github.com/gin-gonic/gin"
)

type Config struct {
	Path    string
	Default bool // 启用默认路由
	Router  DefaultRouterInterface
	View    views.DefaultViewInterface
	Group   *gin.RouterGroup
	Custom  func(*gin.RouterGroup)
}

func RouterGroup(config *Config) {
	if config.Default {
		config.Router.setModelView(config.Path, config.View)
	}
	router_group := config.Group.Group(config.Path)
	router_group.GET("info", config.Router.InfoFunc)
	router_group.POST("add", config.Router.AddFunc)
	router_group.PUT("update", config.Router.UpdateFunc)
	router_group.PATCH("page", config.Router.PageFunc)
	router_group.PATCH("list", config.Router.ListFunc)
	router_group.PATCH("select", config.Router.SelectFunc)
	router_group.PATCH("field", config.Router.FieldFunc)
	router_group.PATCH("upper", config.Router.UpperFunc)
	router_group.PUT("move", config.Router.MoveFunc)
	router_group.PUT("archival", config.Router.ArchivalFunc)
	router_group.DELETE("delete", config.Router.DeleteFunc)
	config.Custom(router_group)
}
