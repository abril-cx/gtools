package gviews

type DefaultParams struct {
	Page  int64  `json:"page"`
	Size  int64  `json:"size"`
	Sort  string `json:"sort"`
	Order string `json:"order"`
}
