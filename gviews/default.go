package gviews

import (
	log "gitee.com/abril-cx/gtools/glog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type RequestData struct {
	Id     primitive.ObjectID
	Model  string
	Maps   bson.M
	Datas  map[string]interface{}
	Params *DefaultParams
	Token  string
	File   *FileHandle
	Files  []*FileHandle
}

type DefaultViewInterface interface {
	InfoFunc(req_data *RequestData) (interface{}, error)
	AddFunc(req_data *RequestData) (interface{}, error)
	UpdateFunc(req_data *RequestData) error
	PageFunc(req_data *RequestData) (interface{}, error)
	ListFunc(req_data *RequestData) (interface{}, error)
	SelectFunc(req_data *RequestData) (interface{}, error)
	FieldFunc(req_data *RequestData) (interface{}, error)
	UpperFunc(req_data *RequestData) (interface{}, error)
	DeleteFunc(req_data *RequestData) error
}

type DefaultView struct{}

func (v *DefaultView) InfoFunc(req_data *RequestData) (interface{}, error) {
	if req_data.Id.IsZero() {
		return nil, log.ErrorReturn("查询数据失败", "id不能为空", *req_data)
	}
	result, err := GetInfo(req_data)
	if err != nil {
		return nil, log.ErrorReturn("查询数据失败", err.Error(), *req_data)
	}
	return result, nil
}

func (v *DefaultView) AddFunc(req_data *RequestData) (interface{}, error) {
	result, err := WriteAdd(req_data)
	if err != nil {
		return result, log.ErrorReturn("写入数据失败", err.Error(), *req_data)
	}
	return result, nil
}

func (v *DefaultView) UpdateFunc(req_data *RequestData) error {
	err := WriteUpdate(req_data)
	if err != nil {
		return log.ErrorReturn("写入数据失败", err.Error(), *req_data)
	}
	return nil
}

func (v *DefaultView) PageFunc(req_data *RequestData) (interface{}, error) {
	result, total, err := GetPage(req_data)
	if err != nil {
		return nil, log.ErrorReturn("查询数据失败", err.Error(), *req_data, *req_data.Params)
	}
	return bson.M{
		"list": result,
		"pagination": bson.M{
			"total": total,
			"page":  req_data.Params.Page,
			"size":  req_data.Params.Size,
		},
	}, nil
}

func (v *DefaultView) ListFunc(req_data *RequestData) (interface{}, error) {
	result, err := GetList(req_data)
	if err != nil {
		return nil, log.ErrorReturn("查询数据失败", err.Error(), *req_data)
	}
	return result, nil
}

func (v *DefaultView) SelectFunc(req_data *RequestData) (interface{}, error) {
	result, err := GetListForSelect(req_data)
	if err != nil {
		return nil, log.ErrorReturn("查询数据失败", err.Error(), *req_data)
	}
	return result, nil
}

func (v *DefaultView) FieldFunc(req_data *RequestData) (interface{}, error) {
	result, err := GetFieldList(req_data)
	if err != nil {
		return nil, log.ErrorReturn("查询数据失败", err.Error(), *req_data)
	}
	return result, nil
}

func (v *DefaultView) UpperFunc(req_data *RequestData) (interface{}, error) {
	result, err := GetUpperList(req_data)
	if err != nil {
		return nil, log.ErrorReturn("查询数据失败", err.Error(), *req_data)
	}
	return result, nil
}

func (v *DefaultView) DeleteFunc(req_data *RequestData) error {
	err := DeleteInfo(req_data)
	if err != nil {
		return log.ErrorReturn("删除数据失败", err.Error(), *req_data)
	}
	return nil
}
