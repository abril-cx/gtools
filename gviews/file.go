package gviews

import (
	"io"
	"mime/multipart"
	"os"
)

type FileHandle struct {
	FileData *multipart.FileHeader
	Suffix   string
}

func (f *FileHandle) Save(dirPath, fileName string) error {
	// 创建保存文件
	os.MkdirAll(dirPath, os.ModePerm)
	destFile, err := os.Create(dirPath + "/" + fileName)
	if err != nil {
		return err
	}
	defer destFile.Close()

	file, err := f.FileData.Open()
	if err != nil {
		return err
	}
	_, err = io.Copy(destFile, file)
	if err != nil {
		return err
	}
	return nil
}
