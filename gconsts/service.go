package gconsts

import (
	"net"
	"time"

	log "gitee.com/abril-cx/gtools/glog"
)

type Service struct {
	WebPort string
	RpcPort string
}

func awaitDependenciesRedis() {
	timeout := time.Duration(1 * time.Second)
	address := COLONY + "-redis-1:6379"
	for {
		_, err := net.DialTimeout("tcp", address, timeout)
		if err == nil {
			break
		}
	}
}

func awaitDependenciesMongo() {
	timeout := time.Duration(1 * time.Second)
	address := COLONY + "-mongo-1:27017"
	for {
		_, err := net.DialTimeout("tcp", address, timeout)
		if err == nil {
			break
		}
	}
}

func AwaitDependencies(services []string) {
	if APP_CLOUD {
		log.Default("等待依赖服务启动...")
		awaitDependenciesRedis()
		awaitDependenciesMongo()
		for _, service := range services {
			timeout := time.Duration(1 * time.Second)
			address := COLONY + "-" + service + "-1:4242"
			for {
				_, err := net.DialTimeout("tcp", address, timeout)
				if err == nil {
					break
				}
			}
			log.Success("依赖服务 [ " + service + " ] 启动完毕")
		}
		log.Success("所有依赖服务启动完毕")
	}
}
