package gmongo

import (
	"go.mongodb.org/mongo-driver/bson"
)

func GetFields(model string) ([]bson.M, error) {
	results := []bson.M{}
	err := Conn.Model("fields").Filter(bson.M{
		"model": model,
	}).Find(&results)
	return results, err
}
