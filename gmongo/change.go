package gmongo

import (
	utils "gitee.com/abril-cx/gtools/gutils"
	"go.mongodb.org/mongo-driver/bson"
)

func FiltrationData(fields []bson.M, data bson.M) bson.M {
	field_keys := []string{}
	for _, v := range fields {
		field_keys = append(field_keys, v["field"].(string))
	}
	for key := range data {
		if !utils.InArrayString(field_keys, key) {
			delete(data, key)
		}
	}
	return data
}
