package gmongo

type ConnModel struct{}

func (c *ConnModel) Model(model string) *ConnHandle {
	return &ConnHandle{
		model: model,
	}
}
