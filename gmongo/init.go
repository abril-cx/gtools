package gmongo

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	SERVICE = "" // APP_NAME (wxbe)
	Host    = "127.0.0.1"
	Port    = "27017"
)

var Conn *ConnModel
var MongoConn *mongo.Database
var Ctx = context.Background()

func Init(db_name string) error {
	if db_name == "" {
		return errors.New("数据库名称不可为空")
	}
	client, err := mongo.Connect(Ctx,
		options.Client().
			// 连接地址
			ApplyURI("mongodb://"+Host+":"+Port).
			// 验证参数
			// SetAuth(
			// 	options.Credential{
			// 		// 用户名
			// 		Username: "root",
			// 		// 密码
			// 		Password: "123456",
			// 	}).
			// // 设置连接数
			SetMaxPoolSize(230))

	if err != nil {
		return err
	}
	err = client.Ping(Ctx, nil)
	if err != nil {
		return err
	}
	MongoConn = client.Database(db_name)
	return nil
}
