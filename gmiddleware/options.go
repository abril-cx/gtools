package gmiddleware

import (
	"github.com/gin-gonic/gin"
)

func Options(ctx *gin.Context) {
	if ctx.Request.Method == "OPTIONS" {
		ctx.Writer.WriteHeader(200)
		return
	} else {
		ctx.Next()
	}
}
