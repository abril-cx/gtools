package gmiddleware

import (
	ResResponse "gitee.com/abril-cx/gtools/gResResponse"
	log "gitee.com/abril-cx/gtools/glog"
	"github.com/gin-gonic/gin"
)

func CustomRecovery(ctx *gin.Context, err interface{}) {
	log.Error("recovery err", err)
	ResResponse.Error(ctx, "系统异常")
}
