package gmiddleware

import (
	"github.com/gin-gonic/gin"
)

var CorsStatus = true

func Cors(ctx *gin.Context) {
	if CorsStatus {
		ctx.Header("Access-Control-Allow-Origin", "*")
		ctx.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE")
		ctx.Header("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Token")
	}
	ctx.Next()
}
