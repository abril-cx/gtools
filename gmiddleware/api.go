package gmiddleware

import (
	"gitee.com/abril-cx/gtools/glog"
	"github.com/gin-gonic/gin"
)

var (
	LogApiPrint = false
	LogApiWrite = false
)

func ApiLog(ctx *gin.Context) {
	method := ctx.Request.Method
	url := ctx.Request.URL.String()
	ip := ctx.ClientIP()
	glog.Api(method, url, ip, LogApiPrint, LogApiWrite)
	ctx.Next()
}
