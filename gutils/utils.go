package gutils

import (
	"math/rand"
	"os"
)

func GetRandNum(length int) string {
	resData := ""
	rand.Seed(GetNowTimeStampNanosecond())
	for i := 0; i < length; i++ {
		resData += IntToStr(rand.Intn(10))
	}
	return resData
}

func CreateDir(dir string) {
	os.MkdirAll(dir, os.ModePerm)
}

func InArrayString(array []string, value string) bool {
	for _, v := range array {
		if v == value {
			return true
		}
	}
	return false
}

func MaxFloat64(data_list []float64) float64 {
	var result float64 = 0.0
	for _, v := range data_list {
		if v > result {
			result = v
		}
	}
	return result
}