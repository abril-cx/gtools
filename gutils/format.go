package gutils

import (
	"os"
	"strings"
)

func FormatFiringArgs() map[string]string {
	data := make(map[string]string)
	key := ""
	value := ""
	for i, v := range os.Args {
		if i == 0 {
			continue
		}
		if strings.Contains(v, "-") {
			if key != "" {
				data[key] = value
				value = ""
			}
			key = v
			if i == len(os.Args)-1 {
				data[key] = ""
			}
		} else if value == "" {
			value = v
			if i == len(os.Args)-1 {
				data[key] = value
			}
		}
	}
	return data
}

func FormatFieldName(name string) string {
	res_name := ""
	capital := []string{
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
		"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
	}
	for i, v := range name {
		value := string(v)
		if i == 0 {
			res_name += strings.ToLower(value)
			continue
		}
		if InArrayString(capital, value) {
			res_name += "_" + strings.ToLower(value)
		} else {
			res_name += value
		}
	}
	return res_name
}
