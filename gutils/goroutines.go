package gutils

import (
	"sync"
)

func runFunc(wg *sync.WaitGroup, key string, handleFunc func(key string)) {
	handleFunc(key)
	wg.Done()
}

func GoAwait(numTh int, handleFunc func(key string)) {
	var wg sync.WaitGroup
	for i := 0; i < numTh; i++ {
		key := IntToStr(i)
		wg.Add(1)
		go runFunc(&wg, key, handleFunc)
	}
	wg.Wait()
}

// func test() {
// 	data := map[string]interface{}{}
// 	GoAwait(10, func(key string) {
// 		log.Debug("开始...", key)
// 	})
// }
