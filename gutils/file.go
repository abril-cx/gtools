package gutils

import (
	"fmt"
	"io/ioutil"
)

func ReadFile(path string, fileName string) ([]byte, error) {
	f, err := ioutil.ReadFile(path + "/" + fileName)
	if err != nil {
		fmt.Println("read fail", err)
		return nil, err
	}
	return f, nil
}
